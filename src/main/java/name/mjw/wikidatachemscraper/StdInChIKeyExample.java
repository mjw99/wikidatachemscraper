package name.mjw.wikidatachemscraper;

import org.wikidata.wdtk.dumpfiles.DumpProcessingController;
import org.wikidata.wdtk.dumpfiles.MwRevision;
import org.wikidata.wdtk.dumpfiles.StatisticsMwRevisionProcessor;

public class StdInChIKeyExample {

	public static void main(String[] args) {

		ExampleHelpers.configureLogging();

		// Controller object for processing dumps:
		DumpProcessingController dumpProcessingController = new DumpProcessingController(
				"wikidatawiki");

		dumpProcessingController.setOfflineMode(true);

		// Example processor for item documents:
		StdInChIKeyDocumentProcessor documentProcessor = new StdInChIKeyDocumentProcessor();

		dumpProcessingController.registerEntityDocumentProcessor(
				documentProcessor, MwRevision.MODEL_WIKIBASE_ITEM, true);

		// Another processor for statistics & time keeping:
		dumpProcessingController.registerMwRevisionProcessor(
				new StatisticsMwRevisionProcessor("statistics", 10000), null,
				true);

		dumpProcessingController.processMostRecentMainDump();
		
		documentProcessor.storeResults();


	}

}
