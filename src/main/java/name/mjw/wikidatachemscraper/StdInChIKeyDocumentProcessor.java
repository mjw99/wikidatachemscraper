package name.mjw.wikidatachemscraper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.wikidata.wdtk.datamodel.interfaces.EntityDocumentProcessor;
import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.PropertyDocument;
import org.wikidata.wdtk.datamodel.interfaces.Statement;
import org.wikidata.wdtk.datamodel.interfaces.StatementGroup;
import org.wikidata.wdtk.datamodel.interfaces.Value;
import org.wikidata.wdtk.datamodel.interfaces.ValueSnak;

public class StdInChIKeyDocumentProcessor extends Object implements

EntityDocumentProcessor {

	long countItems = 0;
	String stdInChIKey = null;
	String svgFileName = null;

	// See http://stackoverflow.com/questions/15036304/wget-wikimedia-image
	final String WIKIDATA_URL_PREFIX = "http://commons.wikimedia.org/wiki/Special:Redirect/file/";

	HashMap<String, String> inchiKeyToUrlMap = new HashMap<String, String>();

	public HashMap<String, String> getInchiKeyToUrlMap() {
		return inchiKeyToUrlMap;

	}

	@Override
	public void processItemDocument(ItemDocument itemDocument) {

		this.countItems++;
		String stdInChIKey = null;
		String svgFileName = null;

		for (StatementGroup sg : itemDocument.getStatementGroups()) {

			// P235 = InchiKey property
			// https://www.wikidata.org/wiki/Property:P235
			if ("P235".equals(sg.getProperty().getId())) {
				for (Statement s : sg.getStatements()) {
					if (s.getClaim().getMainSnak() instanceof ValueSnak) {
						Value v = ((ValueSnak) s.getClaim().getMainSnak())
								.getValue();

						stdInChIKey = v.toString().replace("(String)", "");
						break;
					}
				}

			}
			
			// P117 Chemical Structure
			// https://www.wikidata.org/wiki/Property:P117
			if ("P117".equals(sg.getProperty().getId())) {
				for (Statement s : sg.getStatements()) {
					if (s.getClaim().getMainSnak() instanceof ValueSnak) {
						Value v = ((ValueSnak) s.getClaim().getMainSnak())
								.getValue();

						svgFileName = v.toString().replace("(String)", "");
						break;
					}
				}
			}

		}

		// Only Store SVG file names
		if ( (svgFileName != null) && (stdInChIKey !=null) && (svgFileName.contains(".svg"))) {
			inchiKeyToUrlMap.put(stdInChIKey, WIKIDATA_URL_PREFIX + svgFileName);

			// System.out.println(stdInChIKey + " : " + WIKIDATA_URL_PREFIX +
			// svgFileName);
		}

	}

	@Override
	public void processPropertyDocument(PropertyDocument propertyDocument) {
		// TODO Auto-generated method stub
	}


	public void storeResults() {

		try (PrintStream out = new PrintStream(new FileOutputStream(
				"results.txt"))) {

			for (Map.Entry<String, String> entry : inchiKeyToUrlMap.entrySet()) {
				out.println(entry.getKey() + " : " + entry.getValue());
			}

		} catch (IOException e) {

			System.out.println("Oops");

		}
	}
}
