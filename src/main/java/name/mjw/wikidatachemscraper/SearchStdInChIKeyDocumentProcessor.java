package name.mjw.wikidatachemscraper;

import java.util.HashMap;

import org.wikidata.wdtk.datamodel.interfaces.EntityDocumentProcessor;
import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.PropertyDocument;
import org.wikidata.wdtk.datamodel.interfaces.Statement;
import org.wikidata.wdtk.datamodel.interfaces.StatementGroup;
import org.wikidata.wdtk.datamodel.interfaces.Value;
import org.wikidata.wdtk.datamodel.interfaces.ValueSnak;

public class SearchStdInChIKeyDocumentProcessor extends Object implements

EntityDocumentProcessor {

	long countItems = 0;
	String stdInChIKey = null;
	String svgFileName = null;

	final String WIKIDATA_URL_PREFIX = "http://commons.wikimedia.org/wiki/File:";

	HashMap<String, String> inchiKeyToUrlMap = new HashMap<String, String>();

	public HashMap<String, String> getInchiKeyToUrlMap() {
		return inchiKeyToUrlMap;

	}

	@Override
	public void processItemDocument(ItemDocument itemDocument) {

		String search = "OZNXTQSXSHODFR-UHFFFAOYSA-N";
		this.countItems++;

		for (StatementGroup sg : itemDocument.getStatementGroups()) {

			// P235 = InchiKey property
			// https://www.wikidata.org/wiki/Property:P235
			if ("P235".equals(sg.getProperty().getId())) {
				for (Statement s : sg.getStatements()) {
					if (s.getClaim().getMainSnak() instanceof ValueSnak) {
						Value v = ((ValueSnak) s.getClaim().getMainSnak())
								.getValue();

						stdInChIKey = v.toString().replace("(String)", "");

						if (stdInChIKey.equals(search)) {
							System.out.println(sg);

							break;
						}

					}
				}

			}

		}

	}

	@Override
	public void processPropertyDocument(PropertyDocument propertyDocument) {
		// TODO Auto-generated method stub
	}

}
