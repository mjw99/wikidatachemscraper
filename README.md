# WikiDataChemScraper #

This is a tool that extracts chemical molecules from [WikiData](http://www.wikidata.org) using the [WikiData toolkit](http://www.mediawiki.org/wiki/Wikidata_Toolkit). Specifically, it extracts chemical compounds that have both a [Standard InChI Key](http://en.wikipedia.org/wiki/International_Chemical_Identifier) and a chemical structure diagram in an SVG format.

### Quick start ###

Using the [July 2015](http://dumps.wikimedia.org/wikidatawiki/20150704/) dump :

    $ git clone https://mjw99@bitbucket.org/mjw99/wikidatachemscraper.git
    $
    $ cd wikidatachemscraper
    $ mkdir -p dumpfiles/wikidatawiki/current-20150704
    $ cd dumpfiles/wikidatawiki/current-20150704
    $ wget http://dumps.wikimedia.org/wikidatawiki/20150704/wikidatawiki-20150704-pages-meta-current.xml.bz2
    $ 
    $ cd ../../..
    $ mvn compile
    $ # Run main method in StdInChIKeyExample
    $ mvn exec:java -Dexec.mainClass="name.mjw.wikidatachemscraper.StdInChIKeyExample"

	
Example output can be found [here](https://bitbucket.org/mjw99/wikidatachemscraper/src/HEAD/results.txt)